export type RootStackParamList = {
  Home: undefined;
  Details: {action: string; id?: string};
};
