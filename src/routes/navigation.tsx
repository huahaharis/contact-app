import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from '../pages/Home/HomeScreen';
import DetailsScreen from '../pages/Form/Details';
import {RootStackParamList} from './types';
import { Alert, Image, TouchableOpacity } from 'react-native';

const Stack = createNativeStackNavigator<RootStackParamList>();

const StackNavigator: React.FC = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen 
        name="Home" 
        component={HomeScreen}  
        options={{
          title: 'List Contact',
          headerTitleAlign: 'center',
        }}
        />
      <Stack.Screen name="Details" component={DetailsScreen} />
    </Stack.Navigator>
  );
};

export default StackNavigator;
