import { call } from "redux-saga/effects";

export function* callback(callbackFunc: (res: any) => void = () => null, res: any = null) {
  if (callbackFunc) {
    yield call(callbackFunc, res);
  }
}
