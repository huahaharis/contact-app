import {all} from 'redux-saga/effects';
import {watcherContact} from '../pages/_sagas';

function* rootSaga() {
  yield all([...watcherContact()]);
}

export default rootSaga;
