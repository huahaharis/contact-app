export enum DefaultActionStatus {
  REQUEST = 'request',
  SUCCESS = 'success',
  FAILURE = 'failure',
}

export interface IDefaultRequestParams<T> {
  payload?: T;
  onSuccess?: (res: any) => void;
  onFailure?: (err: any) => void;
}

export interface IDefaultRequestAction<T> extends IDefaultRequestParams<T> {
  type: string;
  actionStatus: DefaultActionStatus;
}

export interface IDefaultSuccessAction<T> {
  type: string;
  data: T;
  actionStatus: DefaultActionStatus;
}

export interface IDefaultFailureAction<T> {
  type: string;
  error: T;
  actionStatus: DefaultActionStatus;
}
