// reducers.ts
import {combineReducers} from 'redux';
import {contactReducer} from '../pages/_reducers';

export const combinedReducers = combineReducers({
  contact: contactReducer,
});
