import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Text, View, TextInput, TouchableOpacity} from 'react-native';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import { RootStackParamList } from '../_types';
import { Styles } from '../styles';
import { actions as contactAction } from '../_actions';

type DetailsScreenProps = NativeStackScreenProps<RootStackParamList, 'Details'>;

const DetailsScreen: React.FC<DetailsScreenProps> = ({route, navigation}) => {
  const {action, id} = route.params;
  const dispatch = useDispatch();
  const detail = useSelector((store: any) => store.contact.detailContact);
  const [fisrtname, onChangeFirstname] = useState("");
  const [lastname, onChangeLastname] = useState('');
  const [age, onChangeAge] = useState('');
  const [photo, onChangePhoto] = useState('');

  useEffect(()=>{
    if(action == "Add" || action == "Edit"){
      navigation.setOptions({ headerTitle: `${action} Form` });
    }else{
      navigation.setOptions({ headerTitle: `${action}` });
    }
console.log(id);

    if(id !== undefined){
      dispatch(
        contactAction.getContactDetail.request({
          payload: id,
          onSuccess: (res: any) => {
            onChangeFirstname(res.data.firstName)
            onChangeLastname(res.data.lastName)
            onChangeAge(JSON.stringify(res.data.age))
            onChangePhoto(res.data.photo)
          },
          onFailure: (err: any) => {
            console.log(err);
          },
        })
      );
    }
  },[dispatch, id])

  const setAge = (e:string) =>{
    if(/^\d+$/.test(e) || e === ''){
      onChangeAge(e)
    }
  }

  const submitContact = () => {
    let obj = {
      "firstName": fisrtname,
      "lastName": lastname,
      "age": parseInt(age),
      "photo": photo
    }
    dispatch(
      contactAction.addContact.request({
        payload: obj,
        onSuccess: (res: any) => {
          console.log(JSON.stringify(res,null,2));
          navigation.push('Home')
          onChangeFirstname("")
          onChangeLastname("")
          onChangeAge("")
          onChangePhoto("")
        },
        onFailure: (err: any) => {
          console.log(JSON.stringify(err,null,2));
        },
      })
    );
  }

  const editContact = () => {
    let obj = {
      "id": id,
      "firstName": fisrtname,
      "lastName": lastname,
      "age": parseInt(age),
      "photo": photo
    }
    dispatch(
      contactAction.editContact.request({
        payload: obj,
        onSuccess: (res: any) => {
          console.log(JSON.stringify(res,null,2));
          navigation.push('Home')
          onChangeFirstname("")
          onChangeLastname("")
          onChangeAge("")
          onChangePhoto("")
        },
        onFailure: (err: any) => {
          console.log(JSON.stringify(err,null,2));
        },
      })
    );
  }

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'grey' }}>
      <View style={{backgroundColor: 'aliceblue', width: '95%', height: 'auto', borderRadius: 10}}>
        <View style={Styles.containerTextinput}>
          <View style={{ flexDirection: 'column' }}>
            <Text style={{color: 'black'}}>What is your Firstname</Text>
            <Text style={{color: 'lightgrey'}}>Please fill with your firstname</Text>
          </View>
          <TextInput
            style={[Styles.input, { width: 130, borderRadius: 10 }]}
            onChangeText={onChangeFirstname}
            value={fisrtname}
            placeholder="Firstname"
            editable={action !== "Detail" ? true : false}
            placeholderTextColor={"black"}
          />
        </View>
        <View style={Styles.containerTextinput}>
          <View style={{ flexDirection: 'column' }}>
            <Text style={{color: 'black'}}>What is your Lastname</Text>
            <Text style={{color: 'lightgrey'}}>Please fill with your lastname</Text>
          </View>
          <TextInput
            style={[Styles.input, { width: 130, borderRadius: 10 }]}
            onChangeText={onChangeLastname}
            value={lastname}
            placeholder="Lastname"
            editable={action !== "Detail" ? true : false}
            placeholderTextColor={"black"}
          />
        </View>
        <View style={Styles.containerTextinput}>
          <View style={{ flexDirection: 'column' }}>
            <Text style={{color: 'black'}}>What is your Age</Text>
            <Text style={{color: 'lightgrey'}}>Please fill with your age</Text>
          </View>
          <TextInput
            style={[Styles.input, { width: 130, borderRadius: 10 }]}
            onChangeText={(e:any)=>setAge(e)}
            value={age}
            keyboardType='numeric'
            placeholder="Age"
            editable={action !== "Detail" ? true : false}
            placeholderTextColor={"black"}
          />
        </View>
        <View style={Styles.containerTextinput}>
          <View style={{ flexDirection: 'column' }}>
            <Text style={{color: 'black'}}>Select your updated Photo</Text>
            <Text style={{color: 'lightgrey'}}>Please select with your photo</Text>
          </View>
          <TextInput
            style={[Styles.input, { width: 130, borderRadius: 10 }]}
            onChangeText={onChangePhoto}
            value={photo}
            editable={action !== "Detail" ? true : false}
            placeholder="Photo"
            placeholderTextColor={"black"}
          />
        </View>
      </View>
      {action !== "Detail" &&(
      <TouchableOpacity style={Styles.btnSubmit} onPress={() => action == "Add" ? submitContact() : editContact()}>
        <Text style={{fontSize: 18, color: '#fff'}}>Submit</Text>
      </TouchableOpacity>
      )}
    </View>
  );
};

export default DetailsScreen;
