import React, { useEffect, useState } from 'react';
import { Text, TouchableOpacity, View, FlatList, Image, TextInput } from 'react-native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { useDispatch, useSelector } from 'react-redux';
import { RootStackParamList } from '../_types';
import { actions as contactAction } from '../_actions';
import { Styles } from '../styles';

type HomeScreenProps = NativeStackScreenProps<RootStackParamList, 'Home'>;

const HomeScreen: React.FC<HomeScreenProps> = ({ navigation }) => {
  const dispatch = useDispatch();
  const listContact = useSelector((store: any) => store.contact.data);
  const [searchArray, setSearchArray] = useState()
  const [search, setSearch] = useState<boolean>(false)
  useEffect(() => {
    dispatch(contactAction.getContact.request({ payload: null }));
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity onPress={()=>OnPressAdd()}>
          <Image 
           source={require('../../assets/icons/add.png')}
           style={{width: 25, height: 25}}
          />
        </TouchableOpacity>
      ),
    });
  }, [dispatch])

  const OnPressAdd = () => {
    navigation.navigate('Details', { action: "Add" });
  };

  const OnPressDetail = (id: string) => {
    navigation.navigate('Details', { action: "Detail", id: id });
  };

  const OnPressDelete = (id: string) => {
    dispatch(
      contactAction.deleteContact.request({
        payload: id,
        onSuccess: (res: any) => {
          console.log(res);
          dispatch(contactAction.getContact.request({ payload: null }));
        },
        onFailure: (err: any) => {
          console.log(err);
        },
      })
    );
  };

  const OnPressEdit = (id: string) => {
    navigation.navigate('Details', { action: "Edit", id: id });
  };

  const searchFun = (text: string) => {
    if (text.length > 0) {
      setSearch(true);
      let penampung: any = [];
      listContact.forEach(function (a: any) {
        if ((a.firstName.toLowerCase().indexOf(text) > -1 || a.lastName.toLowerCase().indexOf(text) > -1)) {
          penampung.push(a);
        }
      });
      setSearchArray(penampung)
    } else {
      setSearch(false);
    }
  }

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <View style={Styles.searchBar}>
        <TextInput 
          placeholderTextColor={"black"}
          placeholder='Search Contact...'
          onChangeText={searchFun}
        />
         <Image 
           source={require('../../assets/icons/searchIcon.png')}
           style={{width: 25, height: 25, marginRight: 20}}
          />
      </View>
      <View style={{flex: 1, flexGrow: 1, width: '90%'}}>
        {search === true ? (
          <FlatList
            data={searchArray}
            renderItem={({ item, index }) => (
              <TouchableOpacity key={index} style={[Styles.card, { alignItems: 'center' }]} onPress={() => OnPressDetail(item.id)}>
                <View style={{ flexDirection: 'column', marginTop: 5, marginLeft: 5 }}>
                  <Text style={{color: 'black'}}>{item.firstName} {item.lastName}</Text>
                </View>
                <View style={{ flexDirection: 'row', marginRight: 5 }}>
                  <TouchableOpacity onPress={() => OnPressEdit(item.id)} style={[Styles.buttonCard, {}]}><Image source={require('../../assets/icons/edit.png')} style={{ width: 25, height: 25 }} /></TouchableOpacity>
                  <TouchableOpacity onPress={() => OnPressDelete(item.id)} style={[Styles.buttonCard, { width: 60, marginLeft: 5 }]}><Image source={require('../../assets/icons/rubbish.png')} style={{ width: 25, height: 25 }} /></TouchableOpacity>
                </View>
              </TouchableOpacity>
            )}
            horizontal={false}
            bounces={false}
            showsVerticalScrollIndicator={true}
            keyExtractor={item => item.id}
            onEndReachedThreshold={1}
            ListEmptyComponent={
              <View
                style={{ flex: 1, alignItems: 'center', marginTop: '40%' }}>
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: 'bold',
                  }}>
                  Contact Not Found
                </Text>
              </View>
            }
          />
        ) : (
          <FlatList
            data={listContact}
            renderItem={({ item, index }) => (
              <TouchableOpacity key={index} style={[Styles.card, { alignItems: 'center' }]} onPress={() => OnPressDetail(item.id)}>
                <View style={{ flexDirection: 'column', marginTop: 5, marginLeft: 5 }}>
                  <Text style={{color: 'black'}}>{item.firstName} {item.lastName}</Text>
                </View>
                <View style={{ flexDirection: 'row', marginRight: 5 }}>
                  <TouchableOpacity onPress={() => OnPressEdit(item.id)} style={[Styles.buttonCard, {}]}><Image source={require('../../assets/icons/edit.png')} style={{ width: 25, height: 25 }} /></TouchableOpacity>
                  <TouchableOpacity onPress={() => OnPressDelete(item.id)} style={[Styles.buttonCard, { width: 60, marginLeft: 5 }]}><Image source={require('../../assets/icons/rubbish.png')} style={{ width: 25, height: 25 }} /></TouchableOpacity>
                </View>
              </TouchableOpacity>
            )}
            horizontal={false}
            bounces={false}
            showsVerticalScrollIndicator={true}
            keyExtractor={item => item.id}
            onEndReachedThreshold={1}
            ListEmptyComponent={
              <View
                style={{ flex: 1, alignItems: 'center', marginTop: '40%' }}>
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: 'bold',
                  }}>
                  Contact Not Found
                </Text>
              </View>
            }
          />
        )}
      </View>
    </View>
  );
};

export default HomeScreen;