import {types as getContact} from './_types';

const initialState = {
  data: null,
  isLoading: true,
  actionStatus: null,
  statusAdd: null,
  statusEdit: null,
  detailContact: {
    "firstName": "",
    "lastName": "",
    "age": "",
    "photo": ""
  }
};

export const contactReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case getContact.GET_CONTACT_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case getContact.GET_CONTACT_FAILED:
      return {
        ...state,
        isLoading: false,
        actionStatus: action.actionStatus,
      };
    case getContact.GET_CONTACT_SUCCESS:
      return {
        ...state,
        data: action.data.data,
        isLoading: false,
      };
    case getContact.DELETE_CONTACT_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case getContact.DELETE_CONTACT_FAILED:
      return {
        ...state,
        isLoading: false,
        actionStatus: action.actionStatus,
      };
    case getContact.DELETE_CONTACT_SUCCESS:
      return {
        ...state,
        data: action.data.data,
        isLoading: false,
      };
    case getContact.ADD_CONTACT_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case getContact.ADD_CONTACT_FAILED:
      return {
        ...state,
        isLoading: false,
        actionStatus: action.actionStatus,
      };
    case getContact.ADD_CONTACT_SUCCESS:
      return {
        ...state,
        statusAdd: action.data.status,
        detailContact: null,
        isLoading: false,
      };
    case getContact.EDIT_CONTACT_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case getContact.EDIT_CONTACT_FAILED:
      return {
        ...state,
        isLoading: false,
        actionStatus: action.actionStatus,
      };
    case getContact.EDIT_CONTACT_SUCCESS:
      return {
        ...state,
        detailContact: null,
        statusEdit: action.data.status,
        isLoading: false,
      };
    case getContact.GET_DETAIL_CONTACT_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case getContact.GET_DETAIL_CONTACT_FAILED:
      return {
        ...state,
        isLoading: false,
        actionStatus: action.actionStatus,
      };
    case getContact.GET_DETAIL_CONTACT_SUCCESS:
      return {
        ...state,
        detailContact: action.data.data,
        isLoading: false,
      };
    default:
      return state;
  }
};
