import {StyleSheet} from 'react-native'

export const Styles = StyleSheet.create({
    container: {
      backgroundColor: 'lightblue',
      width: '30%',
      height: '5%',
      marginBottom: 20,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 10,
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 2},
      shadowOpacity: 0.8,
      shadowRadius: 1,
      borderColor: 'black',
      borderWidth: 1
    },
    card: {
      flex: 1, 
      flexDirection: 'row', 
      backgroundColor: '#fff', 
      margin: 5, 
      borderRadius: 10, 
      height: 55, 
      width: '95%', 
      justifyContent: 'space-between',
      shadowColor: '#000',
      shadowOffset: {width: 0, height: 2},
      shadowOpacity: 0.8,
      shadowRadius: 1,
    },
    buttonCard: {
      borderRadius: 6, 
      height: 20, 
      width: 40, 
      justifyContent: 'center', 
      alignItems: 'center'
    },
    input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      padding: 10,
    },
    containerTextinput: {
      flexDirection: 'row', 
      width: '95%', 
      justifyContent: 'space-between', 
      alignItems: 'center',
      marginLeft: 10
    },
    btnSubmit: { 
      marginTop: 20, 
      width: 375, 
      backgroundColor: '#732182', 
      alignItems: 'center', 
      justifyContent: 'center', 
      maxHeight: 45, 
      height: 45 , 
      borderRadius: 10
    },
    searchBar : {
      width: '85%', 
      backgroundColor: '#fff',
      flexDirection: 'row', 
      marginTop: 5, 
      paddingLeft: 10, 
      borderRadius: 10, 
      alignItems: 'center', 
      marginRight: 5, 
      justifyContent: 'space-between'
    }
});