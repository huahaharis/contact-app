import {put, takeLatest, call} from 'redux-saga/effects';
import {actions as contactAction} from './_actions';
import {types as getContact} from './_types';
import {IDefaultRequestAction} from '../store/defaultTypes';
import HttpService from '../services/http';
import {callback} from '../store/defaultSagas';
import urlsEndpoint from '../services/urlsEndpoint';

export const watcherContact = () => [
  takeLatest(getContact.GET_CONTACT_REQUEST, getAllContact),
  takeLatest(getContact.DELETE_CONTACT_REQUEST, deleteContact),
  takeLatest(getContact.ADD_CONTACT_REQUEST, addContact),
  takeLatest(getContact.EDIT_CONTACT_REQUEST, editContact),
  takeLatest(getContact.GET_DETAIL_CONTACT_REQUEST, getDetailContact),
];

function* getAllContact({
  onSuccess,
  onFailure,
}: IDefaultRequestAction<any>): any {
  const http = new HttpService(urlsEndpoint.GETALLCONTACT);
  try {
    const response = yield call(http.find);
    yield put(contactAction.getContact.success(response));
    yield callback(onSuccess, response);
  } catch (err) {
    yield put(contactAction.getContact.failure(err));
    yield callback(onFailure, err);
  }
}

function* getDetailContact({
  payload,
  onSuccess,
  onFailure,
}: IDefaultRequestAction<any>): any {
  const http = new HttpService(urlsEndpoint.GETALLCONTACT);
  try {
    const response = yield call(http.get, payload);
    yield put(contactAction.getContactDetail.success(response));
    yield callback(onSuccess, response);
  } catch (err) {
    yield put(contactAction.getContactDetail.failure(err));
    yield callback(onFailure, err);
  }
}

function* deleteContact({
  payload,
  onSuccess,
  onFailure,
}: IDefaultRequestAction<any>): any {
  const http = new HttpService(urlsEndpoint.GETALLCONTACT);
  try {
    const response = yield call(http.delete, payload);
    yield put(contactAction.deleteContact.success(response));
    yield callback(onSuccess, response);
  } catch (err) {
    yield put(contactAction.deleteContact.failure(err));
    yield callback(onFailure, err);
  }
}

function* editContact({
  payload,
  onSuccess,
  onFailure,
}: IDefaultRequestAction<any>): any {
  const http = new HttpService(urlsEndpoint.GETALLCONTACT);
  const id = payload.id
  let obj = {
    "firstName": payload.firstName,
    "lastName": payload.lastName,
    "age": parseInt(payload.age),
    "photo": payload.photo
  }
  try {
    const response = yield call(http.put, id, obj);
    yield put(contactAction.editContact.success(response));
    yield callback(onSuccess, response);
  } catch (err) {
    yield put(contactAction.editContact.failure(err));
    yield callback(onFailure, err);
  }
}

function* addContact({
  payload,
  onSuccess,
  onFailure,
}: IDefaultRequestAction<any>): any {
  const http = new HttpService(urlsEndpoint.GETALLCONTACT);
  try {
    const response = yield call(http.post, payload);
    yield put(contactAction.addContact.success(response));
    yield callback(onSuccess, response);
  } catch (err) {
    yield put(contactAction.addContact.failure(err));
    yield callback(onFailure, err);
  }
}
