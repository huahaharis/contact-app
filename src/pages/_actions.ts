import {DefaultActionStatus,IDefaultRequestParams} from '../store/defaultTypes';
import {types as getContact} from './_types';

export const actions = {
  getContact: {
    request: (payload: any) => ({
      type: getContact.GET_CONTACT_REQUEST,
      payload,
      actionStatus: DefaultActionStatus.REQUEST,
    }),
    success: (data: any) => ({
      type: getContact.GET_CONTACT_SUCCESS,
      data,
      actionStatus: DefaultActionStatus.SUCCESS,
    }),
    failure: (error: any) => ({
      type: getContact.GET_CONTACT_FAILED,
      error,
      actionStatus: DefaultActionStatus.FAILURE,
    }),
  },
  getContactDetail: {
    request: (requestData: IDefaultRequestParams<any>) => ({
      type: getContact.GET_DETAIL_CONTACT_REQUEST,
      ...requestData,
      actionStatus: DefaultActionStatus.REQUEST,
    }),
    success: (data: any) => ({
      type: getContact.GET_DETAIL_CONTACT_SUCCESS,
      data,
      actionStatus: DefaultActionStatus.SUCCESS,
    }),
    failure: (error: any) => ({
      type: getContact.GET_DETAIL_CONTACT_FAILED,
      error,
      actionStatus: DefaultActionStatus.FAILURE,
    }),
  },
  deleteContact: {
    request: (requestData: IDefaultRequestParams<any>) => ({
      type: getContact.DELETE_CONTACT_REQUEST,
      ...requestData,
      actionStatus: DefaultActionStatus.REQUEST,
    }),
    success: (data: any) => ({
      type: getContact.DELETE_CONTACT_SUCCESS,
      data,
      actionStatus: DefaultActionStatus.SUCCESS,
    }),
    failure: (error: any) => ({
      type: getContact.DELETE_CONTACT_FAILED,
      error,
      actionStatus: DefaultActionStatus.FAILURE,
    }),
  },
  editContact: {
    request: (requestData: IDefaultRequestParams<any>) => ({
      type: getContact.EDIT_CONTACT_REQUEST,
      ...requestData,
      actionStatus: DefaultActionStatus.REQUEST,
    }),
    success: (data: any) => ({
      type: getContact.EDIT_CONTACT_SUCCESS,
      data,
      actionStatus: DefaultActionStatus.SUCCESS,
    }),
    failure: (error: any) => ({
      type: getContact.EDIT_CONTACT_FAILED,
      error,
      actionStatus: DefaultActionStatus.FAILURE,
    }),
  },
  addContact: {
    request: (requestData: IDefaultRequestParams<any>) => ({
      type: getContact.ADD_CONTACT_REQUEST,
      ...requestData,
      actionStatus: DefaultActionStatus.REQUEST,
    }),
    success: (data: any) => ({
      type: getContact.ADD_CONTACT_SUCCESS,
      data,
      actionStatus: DefaultActionStatus.SUCCESS,
    }),
    failure: (error: any) => ({
      type: getContact.ADD_CONTACT_FAILED,
      error,
      actionStatus: DefaultActionStatus.FAILURE,
    }),
  },
};
