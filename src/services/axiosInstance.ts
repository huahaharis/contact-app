import axios, {AxiosError, AxiosRequestConfig, AxiosResponse} from 'axios';

// import { store } from "../store/configureStore";
// import { actions as authActions } from "../pages/Auth/_actions";
// import { RootState } from "../store/rootReducers";

const ax = axios.create({
  baseURL: 'https://contact.herokuapp.com',
  timeout: 60000,
});

const onRequest = (config: AxiosRequestConfig): AxiosRequestConfig => {
  return {
    ...config,
    headers: {
      ...config.headers,
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
    },
  };
};

const onRequestError = (error: AxiosError): Promise<AxiosError> => Promise.reject(error);

const onResponse = (response: AxiosResponse): AxiosResponse => {
  return response;
};

const onResponseError = (error: AxiosError): Promise<AxiosError> => {
  if (error.response?.status === 401) {
  }
  return Promise.reject(error);
};

ax.interceptors.request.use(onRequest, onRequestError);

ax.interceptors.response.use(onResponse, onResponseError);

export default ax;
